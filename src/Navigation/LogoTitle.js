import React from 'react';
import { Image} from 'react-native';

export default class LogoTitle extends React.Component {
  render() {
    return (
      <Image
        source={require('../Assets/pin.png')}
        style={{ width: 30, height: 30, margin:5, padding: 5, backgroundColor:'red' }}
      />
    );
  }
}
