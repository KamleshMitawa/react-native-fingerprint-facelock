import React from 'react';
import { Button } from 'react-native';
import HomeScreen from '../Home/HomeScreen';
import { createAppContainer, createSwitchNavigator } from 'react-navigation';
import { createStackNavigator } from 'react-navigation-stack';
import Location from '../Home/Location';
import DetailScreen from '../Home/DetailScreen';
import ModalScreen from '../Home/ModalScreen';
import SignIn from '../Home/SignIn';
import SignUp from '../Home/SignUp';
import SettingScreen from '../Home/SettingScreen';
import FeedBack from '../Home/FeedBack';
import ForgetPassword from '../Home/ForgetPassword';
import { createBottomTabNavigator } from 'react-navigation-tabs';
import { createDrawerNavigator } from 'react-navigation-drawer';
import FingerprintPopup from './FingerprintPopup';
import FingerPrintTouch from './FingerPrintTouch';

const LocationStack = createStackNavigator(
  {
    Location: Location
  },
   {
  headerMode: 'none',
  mode: 'modal'

})

const ModalStack = createStackNavigator(
  {
    Modal: ModalScreen
  }, 
  {
  // headerMode: 'none',
  mode: 'modal',
  headerBackTitleVisible: true,
  transparentCard: true
})
const HomeStack = createStackNavigator(
  {
    Home: HomeScreen,
  },
)

const HomePage = createStackNavigator(
  {
    HomeTab: HomeStack,
    LocationScreen: LocationStack,
    Detail: DetailScreen,
  },
  {
    defaultNavigationOptions: {
      headerTintColor: '#fff',
      // headerStyle: {
      //   backgroundColor: 'lightgrey',
      // },
      // headerRight: () => (
      //   <Button
      //     onPress={() => alert('This is a button!')}
      //     title="Info"
      //     color="#fff"
      //   />
      // ),
    },
    navigationOptions: {
      tabBarLabel: 'Home',
    },
  }
);

const DetailStack = createStackNavigator(
  {
    Detail: DetailScreen,
  }
)

const Tabs = createBottomTabNavigator({ Home: HomePage, Details: DetailStack, });

const AuthStack = createSwitchNavigator(
  {
    SignIn: SignIn,
    SignUp: SignUp,
    ForgetPassword: ForgetPassword
  },
  {
    initialRouteName: 'SignIn'
  }
)
const SettingStack = createDrawerNavigator({
  Settings: SettingScreen,
  FeedBack: FeedBack
},
  {
    drawerLabel: 'Menu',
    drawerType: 'front', //below screen content goes in side by slide and in back drawer content in on the back side of screen.
    drawerWidth: 250,
    drawerLockMode: 'unlocked',
    title: 'Drawer-title',
    drawerPosition: 'right',
    drawerBackgroundColor: 'lightgreen',
    overlayColor: 0,
  })

const AppNavigator = createStackNavigator({
  Touch: FingerPrintTouch,
  Auth: AuthStack,
  FingerPrint: FingerprintPopup,
  Setting: SettingStack,
  Home: Tabs,
  Modal: ModalStack
}, 
{
  initialRouteName: 'Touch',
});

const AppContainer = createAppContainer(AppNavigator);
export default AppContainer;