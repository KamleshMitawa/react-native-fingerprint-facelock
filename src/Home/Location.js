import React from 'react';
import { View, Text, Button } from 'react-native';
import Loader from './Loader';
import { withNavigationFocus } from 'react-navigation';
import DetailScreen from './DetailScreen';

class Location extends React.Component {
  static navigationOptions = ({ navigation }) => {
    console.log(navigation.state.params, 'navigation')
    return {
      title: navigation.getParam('name', 'Mitawa'),
      headerMode: 'none',
    };
  };

  state = {
    loading: true
  }

  componentWillUnmount() {
    console.log('componentWillUnmount')
  }
  componentDidMount() {
    console.log('componentDidMount');
    setTimeout(() => {
      this.setState({ loading: false })
    }, 1000)
    let aa = this.props.navigation.isFocused();
    console.log(aa, 'aaaaa', this.props.navigation.state)
    // let dismiss = this.props.navigation.dismiss();
    // console.log('dismiss', dismiss);

    let params = this.props.navigation.state.params;
    console.log('params', params)
  }
  render() {
    console.log(this.props, ' location porps');
    const { loading } = this.state;
    const { isFocused, navigation } = this.props;

    //it fails when params didn't come
    // const { name } = this.props.navigation.state.params; 
    //thats why we use getParams and here Peter is a fallback means when name params is undefined peter is used
    const name = this.props.navigation.getParam('name', 'Peter');

    if (loading) {
      return <Loader color="red" />
    }
    return (
      <View style={{ flex: 1, alignItems: 'center', justifyContent: 'center' }}>
        <Text> Location</Text>
        <Button onPress={() => navigation.goBack(null)} title="Go back anywhere" />
        <Button onPress={() => navigation.popToTop()} title="popToTop" />

        <Button
          accessibilityLabel="Go to location page fro disablities persons"
          color='red'
          title="Go Back To Home"
          onPress={() => this.props.navigation.goBack()}
        // disabled
        />
        <View>
          <Text>{name}</Text>
        </View>
        <Button
          onPress={() => this.props.navigation.setParams({ name: 'LUCY' })}
          title="Set params name to 'LUCY'"
        />
        <Button
          color='red'
          onPress={() => this.props.navigation.push('Detail')}
          title="Go to Detail Screen"
        />
      </View>
    );
  }
}


export default withNavigationFocus(Location);