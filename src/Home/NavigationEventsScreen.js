import React from 'react';
import { View } from 'react-native';
import { NavigationEvents } from 'react-navigation';

const NavigationEventsScreen = () => (
  <View>
    <NavigationEvents
      onWillFocus={payload => console.log('will focus NavigationEventsScreen', payload)}
      onDidFocus={payload => console.log('did focus NavigationEventsScreen', payload)}
      onWillBlur={payload => console.log('will blur NavigationEventsScreen', payload)}
      onDidBlur={payload => console.log('did blur NavigationEventsScreen', payload)}
    />
  </View>
);

export default NavigationEventsScreen;