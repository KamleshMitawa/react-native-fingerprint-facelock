import React from 'react';
import { View, Text, Button, ActivityIndicator } from 'react-native';
import { withNavigation } from 'react-navigation';

class Loader extends React.Component {
  render() {
      const { color= 'green', size= 'large'} = this.props;
    return (
      <View style={{ flex: 1, alignItems: 'center', justifyContent: 'center' }}>
        <ActivityIndicator color={color} size={size}/>
      </View>
    );
  }
}

export default withNavigation(Loader)

