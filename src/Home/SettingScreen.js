import React from 'react';
import { View, Text , Button} from 'react-native';

export default class SettingScreen extends React.Component{
    static navigationOptions = ({ navigation }) => {
        return {
        //   headerMode: 'none',
        };
    }

    render(){
        console.log(this.props, 'SettingScreen drawer props')
        return(
            <View style={{ flex: 1, justifyContent: 'center', aligItems: 'center'}}>
            <Text> SettingScreen</Text>
            <Button color="red" onPress={() => this.props.navigation.navigate('Home')} title="Go to Home After SettingScreen" />
            <Button color="red" onPress={() => this.props.navigation.navigate('ForgetPassword')} title="ForgetPassword" />

            <Button color="red" onPress={() => this.props.navigation.openDrawer()} title="open Drawer" />

            </View>
        )
    }
}