import React from 'react';
import { View, Text, Button, Alert, ActivityIndicator } from 'react-native';
import { withNavigation } from 'react-navigation';

class DetailScreen extends React.Component {
    static navigationOptions = ({ navigation }) => {
        console.log(navigation.state.params, 'navigation')
        return {
          title: navigation.getParam('name', 'A Nested Details Screen'),
        };
      };

  render() {
    const {  navigation } = this.props;
    console.log(this.props, 'porps');

    return (
      <View style={{ flex: 1, alignItems: 'center', justifyContent: 'center' }}>
        <Text>DetailScreen</Text>
        <Button onPress={() => navigation.pop(2)} title="Go to Home" />
        <Button onPress={() =>navigation.replace('Home', { screenName: 'Detail'})} title="Replace Route" />
        <Button onPress={() => navigation.dismiss()} title="Dismiss Stack" />
      </View>
    );
  }
}


export default DetailScreen