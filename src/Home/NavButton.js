import React from 'react';
import { Button } from 'react-native';
import { withNavigation } from 'react-navigation';

class NavButton extends React.Component {
  render() {
      console.log('navigaion props', this.props)
    return (
      <Button
        title="NAV BUTTON"
        onPress={() => {
          this.props.navigation.goBack();
        }}
      />
    );
  }
}

export default withNavigation(NavButton);