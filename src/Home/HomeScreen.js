import React from 'react';
import { View, Text, Button, Alert, ActivityIndicator } from 'react-native';
import Loader from './Loader';
import NavButton from './NavButton';
import NavigationEventsScreen from './NavigationEventsScreen';
import LogoTitle from '../Navigation/LogoTitle';

export default class HomeScreen extends React.Component {
  static navigationOptions = ({ navigation }) => {
    return {
      // headerMode: 'none',
      title: 'Home',
      headerTitle: () => <LogoTitle />,
      headerStyle: {
        backgroundColor: 'grey',
      },
      headerTintColor: '#fff',
      headerTitleStyle: {
        fontWeight: 'bold',
        alignItems: 'flex-start'
      },
      headerRight: () => (
        <Button onPress={() => navigation.navigate('Modal')} title="Open Modal" color={Platform.OS === 'ios' ? "green" : 'red'} />
      )
    };
  };

  state = {
    loading: true
  }

  componentWillUnmount() {
    console.log('componentWillUnmount')
  }
  componentDidMount() {
    console.log('componentDidMount');
    setTimeout(() => {
      this.setState({ loading: false })
    }, 1000)
  }

  render() {
    console.log(this.props, 'porps');
    const { loading } = this.state;
    const { isFocused, navigation } = this.props;

    if (loading) {
      return <Loader color="green" size="small" />
    }
    return (
      <View style={{ flex: 1, alignItems: 'center', justifyContent: 'center' }}>
        <Text>Home Screen</Text>
        <Button onPress={() => navigation.goBack(null)} title="Go back anywhere" />
        <Button
          color='red'
          accessibilityLabel='hiii accessibilityLabel'
          title="Location"
          onPress={() => this.props.navigation.navigate('Location', { name: 'KAMLESH' })}
        />
        <NavButton />
        <NavigationEventsScreen />
      </View>
    );
  }
}


