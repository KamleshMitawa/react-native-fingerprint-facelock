import React from 'react';
import {
  SafeAreaView,
  StatusBar,
  View,
  Text
} from 'react-native';

import AppContainer from './src/Navigation/Navigation';

export default class App extends React.Component {
  render() {
    console.disableYellowBox = true;
    return (
      <>
        <StatusBar barStyle="dark-content" />
        <SafeAreaView
          style={{ backgroundColor: 'red', flex: 1}}
          forceInset={{ top: 'always', horizontal: 'never' }}
        >
          <View style={{ flex: 1}}>
            <AppContainer />
          </View>
        </SafeAreaView>
      </>
    );
  }
}
